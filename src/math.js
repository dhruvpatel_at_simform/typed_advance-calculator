window.fact = function (num) {
    let f = 1;
    let isNag = false;
    let n = parseInt(num);
    if (n < 0) {
        isNag = true;
        n = Math.abs(n);
    }
    for (let i = 1; i <= n; i++) {
        f *= i;
    }
    return isNag ? -f : f;
};
window.inverse = function (num) {
    return num === 0 ? Infinity : 1 / num;
};
const degToRad = (deg = 0) => (Math.PI * deg) / 180;
const radToDeg = (rad = 0) => (rad * 180) / Math.PI;
const evaluateArithmetiExp = (expressionString) => {
    try {
        const result = convertToExp(expressionString)();
        if (result === Infinity || result === -Infinity) {
            throw Error("evaluation is failed, please put valid values");
        }
        if (Number.isNaN(result)) {
            throw Error("you had passed wrong values to functions, please correct.");
        }
        return Number.isInteger(result) ? result : parseFloat(result.toFixed(6));
    }
    catch (error) {
        if (error instanceof Error) {
            return ("Unable to Evaluate Expression, Please insert valid Expression " +
                " ," +
                error.message);
        }
    }
};
const convertToExp = (expressionString) => {
    return new Function(`return ${expressionString}`);
};
export { evaluateArithmetiExp, degToRad, radToDeg };
//# sourceMappingURL=math.js.map