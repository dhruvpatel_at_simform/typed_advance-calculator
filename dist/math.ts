//custom function for finding factorial
type Fact = (num: string) => number;
type Inverse = (num: number) => number;
declare let window: any;
// declare global {
//   interface Window {
//     fact: Fact;
//     inverse: Inverse;
//   }
// }
window.fact = function (num: string) {
  let f = 1;
  let isNag = false;
  let n = parseInt(num);
  if (n < 0) {
    isNag = true;
    n = Math.abs(n);
  }
  for (let i = 1; i <= n; i++) {
    f *= i;
  }
  return isNag ? -f : f;
};

//custom function for finding 1/x or inverse
window.inverse = function (num: number) {
  return num === 0 ? Infinity : 1 / num;
};

const degToRad = (deg = 0) => (Math.PI * deg) / 180;
const radToDeg = (rad = 0) => (rad * 180) / Math.PI;

//main method for evaluating and returning result of expression
const evaluateArithmetiExp: (exp: string) => string | number = (
  expressionString
) => {
  // console.log(expressionString);
  try {
    // console.log(expressionString);
    const result = convertToExp(expressionString)();

    if (result === Infinity || result === -Infinity) {
      throw Error("evaluation is failed, please put valid values");
    }
    if (Number.isNaN(result)) {
      throw Error("you had passed wrong values to functions, please correct.");
    }
    return Number.isInteger(result) ? result : parseFloat(result.toFixed(6));
  } catch (error: unknown) {
    if (error instanceof Error) {
      return (
        "Unable to Evaluate Expression, Please insert valid Expression " +
        " ," +
        error.message
      );
    }
  }
};

const convertToExp = (expressionString: string): Function => {
  return new Function(`return ${expressionString}`);
};

export { evaluateArithmetiExp, degToRad, radToDeg };
