import * as Calc from "./math.js";

//global veriables
type Expression = string;
let expression: Expression = "0";

const bracketInfo: {
  isBracketsOpen: boolean;
  openingCounter: number;
} = {
  isBracketsOpen: false,
  openingCounter: 0,
};

let dotPointer: number | null = null;

type DecimalTracker = {
  expIndx: number;
  dspexpIndx: number;
};

let currentDecimalInitIndexes: DecimalTracker = {
  expIndx: 0,
  dspexpIndx: 0,
};

// selecting require functions

const displayText = document.querySelector("#expression")! as HTMLSpanElement;
const numbers = document.querySelectorAll(".key.num")! as NodeList;
const clearButton = document.querySelector("#clear")! as HTMLButtonElement;
const deleteButton = document.querySelector("#delete")! as HTMLButtonElement;
const openingBracketButton = document.querySelector(
  "#opening-bracket"
)! as HTMLButtonElement;
const closingBracketButton = document.querySelector(
  "#closing-bracket"
)! as HTMLButtonElement;
const additionButton = document.querySelector(
  "#addition-operator"
)! as HTMLButtonElement;
const subtractButton = document.querySelector(
  "#subtraction-operator"
)! as HTMLButtonElement;
const multiplyButton = document.querySelector(
  "#multiplication-operator"
)! as HTMLButtonElement;
const devisionButton = document.querySelector(
  "#devision-operator"
)! as HTMLButtonElement;
const moduloButton = document.querySelector(
  "#modulo-operator"
)! as HTMLButtonElement;
const decimalDotButton = document.querySelector(
  "#decimal-dot"
)! as HTMLButtonElement;
const evaluateButton = document.querySelector(
  "#evaluate-exp"
)! as HTMLButtonElement;
const resultDisplay = document.querySelector(
  "#result-display"
)! as HTMLDivElement;
const expoButton = document.querySelector(
  "#expo-operator"
)! as HTMLButtonElement;
const squareButton = document.querySelector(
  "#square-operator"
)! as HTMLButtonElement;
const tensExpoButton = document.querySelector(
  "#tens-expo"
)! as HTMLButtonElement;
const sqrtButton = document.querySelector(
  "#sqrt-operator"
)! as HTMLButtonElement;
const factorialButton = document.querySelector(
  "#factorial-operator"
)! as HTMLButtonElement;
const inverseButton = document.querySelector(
  "#inverse-operator"
)! as HTMLButtonElement;
const logTenButton = document.querySelector("#log-ten")! as HTMLButtonElement;
const naturalLogButton = document.querySelector(
  "#natural-log"
)! as HTMLButtonElement;
const sinButton = document.querySelector("#sin-function")! as HTMLButtonElement;
const cosButton = document.querySelector("#cos-function")! as HTMLButtonElement;
const tanButton = document.querySelector("#tan-function")! as HTMLButtonElement;
const degToRadInput = document.querySelector("#degree")! as HTMLInputElement;
const radToDegInput = document.querySelector("#radian")! as HTMLInputElement;
// console.log(openingBracket, closingBracket);

// helping methods
const isPreviousCharOperator = () =>
  expression.match(/.*(\+|\-|\*|\/|\%)$/) !== null;
// callback fucntions for event listner
const isDotCanInstert = () => {
  return (
    expression
      .slice(dotPointer ?? 0 + 1, -1)
      .match(/(\+|\-|\*|\/|\%|\(|\))/) !== null
  );
};
function onNumberClicked(num: string): void {
  if (expression[0] === "0" && expression.length === 1) {
    if (num === "0") {
      return;
    } else {
      expression = num;
      displayText.innerText = num;
      return;
    }
  }
  if (
    expression[expression.length - 1] === ")" ||
    expression[expression.length - 1] === "!"
  ) {
    expression += `*${num}`;
    displayText.innerText += `*${num}`;
    return;
  }
  expression += num;
  displayText.innerText += num;
  return;
}

// adding event listner

// adding event listner to all numer and point(.)
numbers.forEach((num: Node, i: number): void =>
  num.addEventListener("click", () =>
    onNumberClicked((num as HTMLButtonElement).innerText)
  )
);

// eventlistner for decimal-dot
decimalDotButton.addEventListener("click", (): void => {
  if (!expression.includes(".")) {
    expression += ".";
    dotPointer = expression.length - 1;
    displayText.innerText += ".";
    return;
  } else {
    // console.log(isDotCanInstert());
    if (isDotCanInstert()) {
      expression += ".";
      dotPointer = expression.length - 1;
      displayText.innerText += ".";
    }
    return;
  }
});

//clear all input
const resetApp = (): void => {
  expression = "0";
  bracketInfo.openingCounter = 0;
  bracketInfo.isBracketsOpen = false;
  dotPointer = null;
  displayText.innerText = "0";
};

clearButton.addEventListener("click", resetApp);

// delete one number at click, backspace
const deleteButtonHandler: () => void = () => {
  if (expression.length === 1) {
    resetApp();
    return;
  } else if (expression[expression.length - 1] === ".") {
    expression = expression.slice(0, -1);
    dotPointer = expression.lastIndexOf(".");
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  } else if (expression[expression.length - 1] === "(") {
    expression = expression.slice(0, -1);
    bracketInfo.openingCounter -= 1;
    if (bracketInfo.openingCounter === 0) bracketInfo.isBracketsOpen = false;
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  } else {
    expression = expression.slice(0, -1);
    displayText.innerText = displayText.innerText.slice(0, -1);
    return;
  }
};

deleteButton.addEventListener("click", deleteButtonHandler);

//event for paranthesis

function openingBracketInfoHandler(): void {
  if (bracketInfo.isBracketsOpen) {
    bracketInfo.openingCounter += 1;
  } else {
    bracketInfo.isBracketsOpen = true;
    bracketInfo.openingCounter = 1;
  }
}

const openingBracketHandler: () => void = () => {
  openingBracketInfoHandler();
  if (displayText.innerText === "0") {
    expression = "(";
    displayText.innerText = expression;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  }
  if (
    (expression[expression.length - 1] >= "0" &&
      expression[expression.length - 1] <= "9") ||
    expression[expression.length - 1] === ")"
  ) {
    // console.log("inside a open para digit");
    expression += "*(";
    displayText.innerText += "*(";
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  }
  // console.log("expression", expression);
  expression += "(";
  displayText.innerText += "(";
  currentDecimalInitIndexes.expIndx = expression.length;
  currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  return;
};

openingBracketButton.addEventListener("click", openingBracketHandler);

const closingBracketHandler: () => void = () => {
  if (
    bracketInfo.isBracketsOpen &&
    expression.charAt(expression.length - 1) !== "("
  ) {
    bracketInfo.openingCounter > 0
      ? (bracketInfo.openingCounter -= 1)
      : (bracketInfo.openingCounter = 0);
    if (bracketInfo.openingCounter === 0) {
      bracketInfo.isBracketsOpen = false;
    }
    expression += ")";
    displayText.innerText += ")";
    return;
  } else {
    return;
  }
};

closingBracketButton.addEventListener("click", closingBracketHandler);

// basic arithmetic operator listner
const isAvoidable: { [key: string]: boolean } = {
  "(": true,
  ".": true,
};

interface ArithmeticCallback {
  (
    expString: string,
    displayExpString: string,
    doHandelParameter?: boolean
  ): void;
}

const arithmaticFunctionCallBack: ArithmeticCallback = (
  expString,
  displayExpString,
  doHandleParameter = true
) => {
  if (doHandleParameter) openingBracketInfoHandler();
  const isAlreadyInserted = checkAndInsert(expString, displayExpString);
  if (isAlreadyInserted === "changed") {
    currentDecimalInitIndexes.expIndx += 2;
    currentDecimalInitIndexes.dspexpIndx += 2;
  }
  if (!isAlreadyInserted) {
    expression += expString;
    displayText.innerText += displayExpString;
  }
  return;
};

const checkAndInsert: (se: string, sed: string) => boolean | "changed" = (
  simpleExp,
  simpleExpDspl
) => {
  if (expression === "0") {
    expression = simpleExp;
    displayText.innerText = simpleExpDspl;
    return true;
  }
  if (
    (expression[expression.length - 1] >= "0" &&
      expression[expression.length - 1] <= "9") ||
    expression[expression.length - 1] === ")"
  ) {
    expression += `*${simpleExp}`;
    displayText.innerText += `*${simpleExpDspl}`;
    return "changed";
  }
  return false;
};

const insertOperator = (operator: string): void => {
  // console.table(bracketInfo);
  let lastChar = expression.charAt(expression.length - 1);
  if (isPreviousCharOperator()) {
    expression = expression.slice(0, -1) + operator;
    displayText.innerText = displayText.innerText.slice(0, -1) + operator;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
    return;
  } else if (isAvoidable[lastChar]) {
    if (operator === "-" && expression[expression.length - 1] === "(") {
      expression += operator;
      displayText.innerText += operator;
      return;
    } else {
      return;
    }
  } else {
    expression += operator;
    displayText.innerText += operator;
    currentDecimalInitIndexes.expIndx = expression.length;
    currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  }
  return;
};

additionButton.addEventListener("click", () => insertOperator("+"));

devisionButton.addEventListener("click", () => insertOperator("/"));

multiplyButton.addEventListener("click", () => insertOperator("*"));

moduloButton.addEventListener("click", () => insertOperator("%"));

subtractButton.addEventListener("click", () => insertOperator("-"));

expoButton.addEventListener("click", () => {
  if (expression[expression.length - 1] === "(") {
    return;
  }
  expression += "**";
  displayText.innerText += "^";
});

squareButton.addEventListener("click", () => {
  if (expression[expression.length - 1] === "(") {
    return;
  }
  expression += "**2";
  displayText.innerText += "^2";
});

tensExpoButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("10**", "10^", false)
);

sqrtButton.addEventListener("click", () => {
  openingBracketInfoHandler();
  const isAlreadyInserted = checkAndInsert("Math.sqrt(", "√(");
  if (!isAlreadyInserted) {
    expression += "Math.sqrt(";
    displayText.innerText += "√(";
  }
  currentDecimalInitIndexes.expIndx = expression.length;
  currentDecimalInitIndexes.dspexpIndx = displayText.innerText.length;
  return;
});

factorialButton.addEventListener("click", () => {
  // console.log(currentDecimalInitIndexes);
  let lastChar = expression[expression.length - 1];
  if (lastChar === "^") return;
  expression =
    expression.substring(0, currentDecimalInitIndexes.expIndx) +
    `fact(${expression.substring(currentDecimalInitIndexes.expIndx)})`;
  displayText.innerText += "!";
  return;
});

inverseButton.addEventListener("click", () => {
  // console.log(currentDecimalInitIndexes);
  let lastChar = expression[expression.length - 1];
  if (lastChar === "^") return;
  expression =
    expression.substring(0, currentDecimalInitIndexes.expIndx) +
    `inverse(${expression.substring(currentDecimalInitIndexes.expIndx)})`;
  displayText.innerText =
    displayText.innerText.substring(0, currentDecimalInitIndexes.dspexpIndx) +
    `(1/${displayText.innerText.substring(
      currentDecimalInitIndexes.dspexpIndx
    )})`;
});

//logarethmic function
naturalLogButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.log(", "ln(")
);

logTenButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.log10(", "log(")
);

//trigonmetric function
sinButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.sin(", "sin(")
);

cosButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.cos(", "cos(")
);

tanButton.addEventListener("click", () =>
  arithmaticFunctionCallBack("Math.tan(", "tan(")
);

// Degree to Radian and vice-versa even listner

degToRadInput.addEventListener("input", (e: Event) => {
  const { target } = e;
  if (target) {
    // type guarding the event beacuse event could be null.
    if ((target as HTMLInputElement).value !== "") {
      // telling ts that it's input type so it can have value and other attribute unlike only 3 core methods
      radToDegInput.setAttribute(
        "value",
        `${Calc.degToRad(parseInt((target as HTMLInputElement).value))}`
      );
      return;
    } else {
      radToDegInput.setAttribute("value", "");
    }
  }
});

// equal -sign means evaluate equation button eventlistner
evaluateButton.addEventListener("click", () => {
  if (expression === "0") {
    return;
  }
  if (bracketInfo.isBracketsOpen) {
    for (let i = 0; i < bracketInfo.openingCounter; i++) {
      expression += ")";
      displayText.innerText += ")";
    }
  }
  const result = Calc.evaluateArithmetiExp(expression);

  if (typeof result === "string") {
    alert(result + " : " + displayText.innerText);
  } else {
    resultDisplay.innerHTML = `<span class="exp">${displayText.innerText}</span><span class="equal-sign">=</span><span class="output">${result}</span>`;
    expression = result.toString();
    displayText.innerText = expression;
    bracketInfo.isBracketsOpen = false;
    bracketInfo.openingCounter = 0;
    currentDecimalInitIndexes.expIndx = 0;
    currentDecimalInitIndexes.dspexpIndx = 0;
    // for adding focus style around display
    (
      document.querySelector(".expression-cls")! as HTMLSpanElement
    ).classList.add("highlight");
    setTimeout(() => {
      (
        document.querySelector(".expression-cls.highlight")! as HTMLSpanElement
      ).classList.remove("highlight");
    }, 1500);
  }
});
